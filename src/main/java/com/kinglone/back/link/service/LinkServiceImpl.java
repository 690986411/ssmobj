package com.kinglone.back.link.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kinglone.back.link.dao.LinkMapper;
import com.kinglone.back.link.model.Link;

@Service("linkService")
public class LinkServiceImpl implements LinkService {

	@Autowired
	private LinkMapper linkDao;

	@Override
	public List<Link> list(Map<String, Object> map) {
		return linkDao.list(map);
	}

	@Override
	public Integer getTotal(Map<String, Object> map) {
		return linkDao.getTotal(map);
	}

	@Override
	public Integer save(Link link) {
		return linkDao.save(link);
	}

	@Override
	public Integer update(Link link) {
		return linkDao.update(link);
	}

	@Override
	public Integer delete(Integer id) {
		return linkDao.delete(id);
	}
}
