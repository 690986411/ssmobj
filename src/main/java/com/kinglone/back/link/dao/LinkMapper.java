package com.kinglone.back.link.dao;

import java.util.List;
import java.util.Map;

import com.kinglone.back.link.model.Link;

public interface LinkMapper {
 
    /**
     * 分页查询友情链接信息
     * @param map
     * @return
     */
	public List<Link> list(Map<String, Object> map);
	/**
	 * 查询友情链接总数
	 * @param map
	 * @return
	 */
	public Integer getTotal(Map<String, Object> map);
	/**
	 * 添加友情链接
	 * @param link
	 * @return
	 */
	public Integer save(Link link);
	/**
	 * 修改友情链接
	 * @param link
	 * @return
	 */
	public Integer update(Link link);
	/**
	 * 删除友情链接
	 * @param id
	 * @return
	 */
	public Integer delete(Integer id);
}