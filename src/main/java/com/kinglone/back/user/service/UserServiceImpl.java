package com.kinglone.back.user.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.kinglone.back.user.dao.UserMapper;
import com.kinglone.back.user.model.User;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Resource
	private UserMapper userDao;
	
	@Override
	public User getByUserName(String accountName) {
		
		return userDao.findUserByUserName(accountName);
	}

	@Override
	public Integer update(User user) {
		return userDao.update(user);
	}

}
