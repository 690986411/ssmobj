package com.kinglone.back.user.service;

import com.kinglone.back.user.model.User;

public interface UserService {

	/**
	 * 根据用户名查询用户
	 * @param accountName
	 * @return
	 */
	User getByUserName(String accountName);

	/**
	 * 更新博主信息
	 * @param user
	 * @return
	 */
	Integer update(User user);

}
