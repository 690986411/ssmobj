package com.kinglone.back.user.model;

public class User {
    private String sysid; //id
    private String imagename;//博主头像
    private String nickname;//昵称
    private String password;//密码
    private String profile;//个人简介
    private String sign;//个性签名
    private String username;//用户名
    
    public User() {}
	public User(String sysid, String imagename, String nickname,
			String password, String profile, String sign, String username) {
		super();
		this.sysid = sysid;
		this.imagename = imagename;
		this.nickname = nickname;
		this.password = password;
		this.profile = profile;
		this.sign = sign;
		this.username = username;
	}

	public String getSysid() {
        return sysid;
    }

    public void setSysid(String sysid) {
        this.sysid = sysid == null ? null : sysid.trim();
    }

    public String getImagename() {
        return imagename;
    }

    public void setImagename(String imagename) {
        this.imagename = imagename == null ? null : imagename.trim();
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname == null ? null : nickname.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile == null ? null : profile.trim();
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign == null ? null : sign.trim();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }
}