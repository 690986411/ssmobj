package com.kinglone.back.user.dao;

import com.kinglone.back.user.model.User;

public interface UserMapper {

    /**
     * 根据用户名查询用户
     * @param accountName
     * @return
     */
	public User findUserByUserName(String accountName);

	public Integer update(User user);
}