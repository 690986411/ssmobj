package com.kinglone.back.comment.dao;

import java.util.List;
import java.util.Map;

import com.kinglone.back.comment.model.Comment;

public interface CommentMapper {

    /**
     * 分页查询评论列表
     * @param map
     * @return
     */
	public List<Comment> list(Map<String, Object> map);
	/**
	 * 查询评论列表总数
	 * @param map
	 * @return
	 */
	public Long getTotal(Map<String, Object> map);

	/**
	 * 根据id删除评论
	 * @param id
	 * @return
	 */
	public Integer delete(Integer id);
	/**
	 * 修改评论
	 * @param comment
	 * @return
	 */
	public Integer update(Comment comment);
	/**
	 * 新增评论
	 * @param comment
	 * @return
	 */
	public Integer add(Comment comment);
}