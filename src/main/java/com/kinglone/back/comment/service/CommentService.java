package com.kinglone.back.comment.service;

import java.util.List;
import java.util.Map;

import com.kinglone.back.comment.model.Comment;

public interface CommentService {

	/**
	 * 分页查询评论列表
	 * @param map
	 * @return
	 */
	public List<Comment> list(Map<String, Object> map);

	/**
	 * 查询评论总数
	 * @param map
	 * @return
	 */
	public Long getTotal(Map<String, Object> map);
	/**
	 * 删除评论
	 * @param parseInt
	 */
	public Integer delete(Integer id);

	/**
	 * 修改评论
	 * @param comment
	 */
	public Integer update(Comment comment);
	/**
	 * 添加评论
	 * @param comment
	 * @return
	 */
	public Integer add(Comment comment);

}
