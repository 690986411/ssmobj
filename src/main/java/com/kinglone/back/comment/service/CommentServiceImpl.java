package com.kinglone.back.comment.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kinglone.back.comment.dao.CommentMapper;
import com.kinglone.back.comment.model.Comment;

@Service("commentService")
public class CommentServiceImpl implements CommentService {

	@Autowired
	private CommentMapper commentDao;

	@Override
	public List<Comment> list(Map<String, Object> map) {
		return commentDao.list(map);
	}

	@Override
	public Long getTotal(Map<String, Object> map) {
		return commentDao.getTotal(map);
	}

	@Override
	public Integer delete(Integer id) {
		return commentDao.delete(id);
	}

	@Override
	public Integer update(Comment comment) {
		return commentDao.update(comment);
	}

	@Override
	public Integer add(Comment comment) {
		return commentDao.add(comment);
	}
}
