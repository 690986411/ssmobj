package com.kinglone.back.system;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kinglone.back.blog.model.Blog;
import com.kinglone.back.blog.service.BlogService;
import com.kinglone.back.blogType.model.BlogType;
import com.kinglone.back.blogType.service.BlogTypeService;
import com.kinglone.back.link.model.Link;
import com.kinglone.back.link.service.LinkService;
import com.kinglone.back.music.model.Music;
import com.kinglone.back.music.service.MusicService;
import com.kinglone.common.utils.ResponseUtil;

/**
 * 刷新系统缓存
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/back/system")
public class SystemController {
	
	@Autowired
	private BlogTypeService blogTypeService;
	@Autowired
	private BlogService blogService;
	@Autowired
	private LinkService linkService;
	@Autowired
	private MusicService musicService;

	@RequestMapping("/refreshSystem")
	public String refreshSystem(HttpServletRequest request,HttpServletResponse response) throws Exception{
		
		ServletContext application = request.getSession().getServletContext();
		
		List<BlogType> blogTypeCountList=blogTypeService.countList(); // 查询博客类别以及博客的数量
		application.setAttribute("blogTypeCountList", blogTypeCountList);  
		
		List<Blog> blogCountList=blogService.countList(); // 根据日期分组查询博客
		application.setAttribute("blogCountList", blogCountList);
		
		List<Link> linkList= linkService.list(null);//查询友情链接
		application.setAttribute("linkList", linkList);
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("ifUse", 0);
		List<Music> musicList= musicService.list(map);//查询音乐
		JSONArray jsonArray = new JSONArray();
		for (int i = 0;i<musicList.size();i++){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("url",musicList.get(i).getMusicUrl());
            jsonObject.put("name",musicList.get(i).getMusicName());
            jsonArray.add(jsonObject);
        }
		application.setAttribute("MusicData", jsonArray);
		
		JSONObject result = new JSONObject();
		result.put("success", true);
		ResponseUtil.write(response, result);
		return null;
	}
}
