package com.kinglone.back.blog.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kinglone.back.blog.dao.BlogMapper;
import com.kinglone.back.blog.model.Blog;

@Service("blogService")
public class BlogServiceImpl implements BlogService {

	@Autowired
	private BlogMapper blogDao;
	
	@Override
	public List<Blog> list(Map<String, Object> map) {
		return blogDao.list(map);
	}

	@Override
	public Long getTotal(Map<String, Object> map) {
		return blogDao.getTotal(map);
	}

	@Override
	public Integer add(Blog blog) {
		return blogDao.add(blog);
	}

	@Override
	public Blog findBlogById(Integer id) {
		return blogDao.findBlogById(id);
	}

	@Override
	public Integer update(Blog blog) {
		return blogDao.update(blog);
	}

	@Override
	public Integer delete(Integer id) {
		return blogDao.delete(id);
	}

	@Override
	public Integer findBlogByTypeId(Integer typeId) {
		return blogDao.findBlogByTypeId(typeId);
	}

	@Override
	public List<Blog> countList() {
		return blogDao.countList();
	}

	@Override
	public Blog getLastBlog(Integer id) {
		return blogDao.getLastBlog(id);
	}

	@Override
	public Blog getNextBlog(Integer id) {
		return blogDao.getNextBlog(id);
	}

}
