package com.kinglone.back.blog.service;

import java.util.List;
import java.util.Map;

import com.kinglone.back.blog.model.Blog;

public interface BlogService {

	/**
	 * 分页查询博客
	 * @param map
	 * @return
	 */
	public List<Blog> list(Map<String, Object> map);

	/**
	 * 查询博客数量
	 * @param map
	 * @return
	 */
	public Long getTotal(Map<String, Object> map);

	/**
	 * 添加博客
	 * @param blog
	 * @return
	 */
	public Integer add(Blog blog);

	/**
	 * 根据id查询博客
	 * @param id
	 * @return
	 */
	public Blog findBlogById(Integer id);
	
	/**
	 * 根据id修改博客
	 * @param blog
	 * @return
	 */
	public Integer update(Blog blog);
	
	/**
	 * 根据id删除博客
	 * @param valueOf
	 */
	public Integer delete(Integer id);
	
	/**
	 * 根据博客类型id查询博客
	 * @param valueOf
	 * @return
	 */
	public Integer findBlogByTypeId(Integer typeId);

	/**
	 * 根据日期分组查询博客
	 * @return
	 */
	public List<Blog> countList();
	
	/**
	 * 获取上一篇文章
	 * @param id
	 * @return
	 */
	public Blog getLastBlog(Integer id);
	/**
	 * 获取下一篇文章
	 * @param id
	 * @return
	 */
	public Blog getNextBlog(Integer id);

}
