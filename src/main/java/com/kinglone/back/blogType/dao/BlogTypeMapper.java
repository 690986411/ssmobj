package com.kinglone.back.blogType.dao;

import java.util.List;
import java.util.Map;

import com.kinglone.back.blogType.model.BlogType;

public interface BlogTypeMapper {
    
    /**
     * 分页查询博客类型
     * @param map
     * @return
     */
	public List<BlogType> list(Map<String, Object> map);

	/**
	 * 查询博客类型总数
	 * @param map
	 * @return
	 */
	public Long getTotal(Map<String, Object> map);

	/**
	 * 添加博客类型
	 * @param blogType
	 * @return
	 */
	public Integer add(BlogType blogType);

	/**
	 * 修改博客类型
	 * @param blogType
	 * @return
	 */
	public Integer update(BlogType blogType);

	/**
	 * 根据id删除博客类型
	 * @param id
	 * @return
	 */
	public Integer delete(Integer id);
	
	/**
	 * 通过id查询博客类型
	 * @param id
	 * @return
	 */
	public BlogType findById(Integer id);
	
	/**
	 * 查询博客类别以及博客的数量
	 * @return
	 */
	public List<BlogType> countList();
}