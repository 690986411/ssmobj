package com.kinglone.back.blogType.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.kinglone.back.blog.service.BlogService;
import com.kinglone.back.blogType.model.BlogType;
import com.kinglone.back.blogType.service.BlogTypeService;
import com.kinglone.common.entity.PageBean;
import com.kinglone.common.utils.ResponseUtil;

@Controller
@RequestMapping("/back/blogType")
public class BlogTypeController {

	@Autowired
	private BlogTypeService blogTypeService;
	
	@Autowired
	private BlogService blogService;

	/**
	 * 分页查询博客类别信息
	 * 
	 * @param page
	 * @param rows
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/list")
	public String list(@RequestParam(value = "page", required = false) String page,@RequestParam(value = "rows", required = false) String rows,
			HttpServletResponse response) throws Exception {

		PageBean pageBean = new PageBean(Integer.parseInt(page),Integer.parseInt(rows));

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("start", pageBean.getStart());
		map.put("size", pageBean.getPageSize());

		List<BlogType> blogTypeList = blogTypeService.list(map);
		Long total = blogTypeService.getTotal(map);
		JSONObject result = new JSONObject();
		JSONArray jsonArray = JSONArray.fromObject(blogTypeList);
		result.put("rows", jsonArray);
		result.put("total", total);
		ResponseUtil.write(response, result);
		return null;
	}

	/**
	 * 添加或修改博客类型
	 * 
	 * @param blogType
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/save")
	public String save(BlogType blogType, HttpServletResponse response)throws Exception {
		
		int resultTotal = 0;// 操作记录数
		if (blogType.getId() == null) {
			resultTotal = blogTypeService.add(blogType);
		} else {
			resultTotal = blogTypeService.update(blogType);
		}
		JSONObject result = new JSONObject();
		if (resultTotal > 0) {
			result.put("success", true);
		} else {
			result.put("success", false);
		}
		ResponseUtil.write(response, result);
		return null;

	}
	/**
	 * 删除博客类型
	 * @param ids
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/delete")
	public String delete(@RequestParam(value="ids") String ids,HttpServletResponse response)throws Exception{
		String[] idsStr = ids.split(",");
		JSONObject result = new JSONObject();
		for(int i=0;i<idsStr.length;i++){
			//判断该博客类型下是否有关联博客，如果有不允许删除
			if(blogService.findBlogByTypeId(Integer.valueOf(idsStr[i]))>0){
				result.put("message", "博客类别下有博客，不能删除！");
			}else{
				blogTypeService.delete(Integer.parseInt(idsStr[i]));
			}	
		}
		result.put("success", true);
		ResponseUtil.write(response, result);
		return null;
	}
}
