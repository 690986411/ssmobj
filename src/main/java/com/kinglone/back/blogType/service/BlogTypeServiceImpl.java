package com.kinglone.back.blogType.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kinglone.back.blogType.dao.BlogTypeMapper;
import com.kinglone.back.blogType.model.BlogType;

@Service("blogTypeService")
public class BlogTypeServiceImpl implements BlogTypeService {

	@Autowired
	private BlogTypeMapper blogTypeDao;
	
	@Override
	public List<BlogType> list(Map<String, Object> map) {		
		return blogTypeDao.list(map);
	}

	@Override
	public Long getTotal(Map<String, Object> map) {
		return blogTypeDao.getTotal(map);
	}

	@Override
	public Integer add(BlogType blogType) {
		return blogTypeDao.add(blogType);
	}

	@Override
	public Integer update(BlogType blogType) {
		return blogTypeDao.update(blogType);
	}

	@Override
	public Integer delete(Integer id) {
		return blogTypeDao.delete(id);
	}

	@Override
	public List<BlogType> countList() {
		return blogTypeDao.countList();
	}

}
