package com.kinglone.back.blogType.service;

import java.util.List;
import java.util.Map;

import com.kinglone.back.blogType.model.BlogType;

public interface BlogTypeService {

	/**
	 * 分页查询数据
	 * @param map
	 * @return
	 */
	public List<BlogType> list(Map<String, Object> map);

	/**
	 * 查询总数量
	 * @param map
	 * @return
	 */
	public Long getTotal(Map<String, Object> map);

	/**
	 * 添加博客类型
	 * @param blogType
	 * @return
	 */
	public Integer add(BlogType blogType);

	/**
	 * 修改博客类型
	 * @param blogType
	 * @return
	 */
	public Integer update(BlogType blogType);

	/**
	 * 删除博客类型
	 * @param parseInt
	 */
	public Integer delete(Integer parseInt);

	/**
	 * 查询博客类别以及博客的数量
	 * @return
	 */
	public List<BlogType> countList();

}
