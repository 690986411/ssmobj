package com.kinglone.back.music.service;

import java.util.List;
import java.util.Map;

import com.kinglone.back.music.model.Music;

public interface MusicService {

	/**
	 * 分页查询音乐列表
	 * @param map
	 * @return
	 */
	public List<Music> list(Map<String, Object> map);
	/**
	 * 查询音乐总数
	 * @param map
	 * @return
	 */
	public Long getTotal(Map<String, Object> map);
	/**
	 * 添加音乐
	 * @param music
	 * @return
	 */
	public Integer save(Music music);
	/**
	 * 删除音乐
	 * @param parseInt
	 */
	public Integer delete(Integer id);
	/**
	 * 根据id查询音乐
	 * @param i
	 * @return
	 */
	public String getMusicById(Integer id);
	/**
	 * 根据id编辑音乐
	 * @param music
	 * @return
	 */
	public Integer update(Music music);

}
