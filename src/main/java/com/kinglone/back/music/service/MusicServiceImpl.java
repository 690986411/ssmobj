package com.kinglone.back.music.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kinglone.back.music.dao.MusicMapper;
import com.kinglone.back.music.model.Music;

@Service("musicService")
public class MusicServiceImpl implements MusicService {

	@Autowired
	private MusicMapper musicDao;

	@Override
	public List<Music> list(Map<String, Object> map) {
		return musicDao.list(map);
	}

	@Override
	public Long getTotal(Map<String, Object> map) {
		return musicDao.getTotal(map);
	}

	@Override
	public Integer save(Music music) {
		return musicDao.save(music);
	}

	@Override
	public Integer delete(Integer id) {
		return musicDao.delete(id);
	}

	@Override
	public String getMusicById(Integer id) {
		return musicDao.getMusicById(id);
	}

	@Override
	public Integer update(Music music) {
		return musicDao.update(music);
	}
}
