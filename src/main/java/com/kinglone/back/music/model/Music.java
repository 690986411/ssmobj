package com.kinglone.back.music.model;

/**
 * 音乐实体
 * @author Administrator
 *
 */
public class Music {
    private Integer id; //编号
    private String musicName; //音乐名称
    private String musicUrl; //音乐路径
    private Integer ifUse; //是否使用  0表示使用， 1表示不使用
    private Integer orderNo; //排序编号

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

	public String getMusicName() {
		return musicName;
	}

	public void setMusicName(String musicName) {
		this.musicName = musicName;
	}

	public String getMusicUrl() {
		return musicUrl;
	}

	public void setMusicUrl(String musicUrl) {
		this.musicUrl = musicUrl;
	}

	public Integer getIfUse() {
		return ifUse;
	}

	public void setIfUse(Integer ifUse) {
		this.ifUse = ifUse;
	}

	public Integer getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(Integer orderNo) {
		this.orderNo = orderNo;
	}

   
}