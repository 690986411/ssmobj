package com.kinglone.common.utils;

import org.apache.shiro.crypto.hash.Md5Hash;

/**
 * 加密工具
 * @author Administrator
 *
 */
public class CryptoUtil {

	public static String md5(String pws,String salt){
		
		return new Md5Hash(pws, salt).toString();
	}
	
	public static void main(String[] args){
		String password = "admin";
		System.out.println("MD5加密："+CryptoUtil.md5(password, "kinglone"));
	}
}
