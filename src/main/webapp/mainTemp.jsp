<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>${pageTitle}- kinglone - 个人博客</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="kinglone" name="copyright" />
<meta content="www.kinglone.com" name="author" />
<meta content=${keywords} name="keywords" />
<meta content=${description } name="description" />

<link rel="stylesheet" href="${pageContext.request.contextPath}/static/bootstrap3/css/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/bootstrap3/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/blog.css">
<link href="${pageContext.request.contextPath}/favicon.ico" rel="SHORTCUT ICON">
<script src="${pageContext.request.contextPath}/static/bootstrap3/js/jquery-1.11.2.min.js"></script>
<script src="${pageContext.request.contextPath}/static/bootstrap3/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/common.js"></script>
<%-- <script src="${pageContext.request.contextPath}/static/js/index.js"></script> --%>
<script src="${pageContext.request.contextPath}/static/js/jquery.cookie.min.js"></script>

<script type="text/javascript">
	var _hmt = _hmt || [];
	(function() {
	  var hm = document.createElement("script");
	  hm.src = "//hm.baidu.com/hm.js?aa5c701f4f646931bf78b6f40b234ef5";
	  var s = document.getElementsByTagName("script")[0]; 
	  s.parentNode.insertBefore(hm, s);
	})();
	
	
    var bgmList = <% out.print(application.getAttribute("MusicData")); %>;
    console.log(bgmList);
    var base_url = "${pageContext.request.contextPath}";
</script>

<style type="text/css">
	  body {
        padding-top: 10px;
        padding-bottom: 40px;
      }     
</style>
</head>
<body>
<div class="container">
	<jsp:include page="/front/head.jsp"/>
	
	<jsp:include page="/front/menu.jsp"/>
	
	<div class="row">
		<div class="col-md-9">
			<jsp:include page="${mainPage}"></jsp:include>
		</div>
		
		<div class="col-md-3">
			<div class="data_list">
				<div class="data_list_title">
					<img src="${pageContext.request.contextPath}/static/images/music.png "/>
					音乐
				</div>
				<div class="slideItem">
		            <p class="slideItemTitle" style="position: relative;overflow: hidden;">音乐 - <span id="musicLabel" style="color: #999;font-weight: normal;font-size: 12px;display: inline-block;vertical-align: middle;"></span><a href="javascript:;" style="vertical-align: middle; position: relative;top: 2px; font-weight: normal; font-size: 12px;color: #666;display: inline-block;float: right;" onclick="randomBGM()">切歌</a></p>
		            <div class="slideItemContent musicBox">
		                <audio id="bgmAudio" controls="controls" style="width: 236px;" autoplay>
		                    <source src="" type="audio/mpeg" />
		                    Your browser does not support the audio element.bgmObject
		                </audio>
		            </div>
        		</div>
			</div>
			
			<div class="data_list">
				<div class="data_list_title">
					<img src="${pageContext.request.contextPath}/static/images/byType_icon.png"/>
					按日志类别
				</div>
				<div class="datas">
					<ul>
						<c:forEach var="blogTypeCount" items="${blogTypeCountList }">
							<li><span><a href="${pageContext.request.contextPath}/index.html?typeId=${blogTypeCount.id}">${blogTypeCount.typeName }(${blogTypeCount.blogCount })</a></span></li>
						</c:forEach>
					</ul>
				</div>
			</div>
			
			<div class="data_list">
				<div class="data_list_title">
					<img src="${pageContext.request.contextPath}/static/images/byDate_icon.png"/>
					按日志日期
				</div>
				<div class="datas">
					<ul>
						<c:forEach var="blogCount" items="${blogCountList}">
							<li><span><a href="${pageContext.request.contextPath}/index.html?releaseDateStr=${blogCount.releaseDateStr}">${blogCount.releaseDateStr}(${blogCount.blogCount})</a></span></li>
						</c:forEach>
					</ul>
				</div>
			</div>
			
			<div class="data_list">
				<div class="data_list_title">
					<img src="${pageContext.request.contextPath}/static/images/link_icon.png"/>
					友情链接
				</div>
				<div class="datas">
					<ul>
						<c:forEach var="link" items="${linkList}">
							<li><span><a href="${link.linkUrl}" target="_blank">${link.linkName}</a></span></li>
						</c:forEach>
					</ul>
				</div>
			</div>			
		</div>		
	</div>	
	<jsp:include page="/front/foot.jsp"/>
</div>
</body>
</html>