<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>修改博客页面</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/jquery-easyui-1.3.3/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/jquery-easyui-1.3.3/themes/icon.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/static/jquery-easyui-1.3.3/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/jquery-easyui-1.3.3/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/jquery-easyui-1.3.3/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/back_js/writeBlog.js"></script>
<%--修正上传问题用的插件--%>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery.form.js"></script>
<script type="text/javascript">var contextPath="${pageContext.request.contextPath}";</script>
</head>
<body style="margin: 10px">
<div id="p" class="easyui-panel" title="修改博客" style="padding: 10px">
 	<table cellspacing="20px">
   		<tr>
   			<td width="80px">博客标题：</td>
   			<td><input type="text" id="title" name="title" style="width: 400px;"/></td>
   		</tr>
   		<tr>
   			<td>所属类别：</td>
   			<td>
   				<select class="easyui-combobox" style="width: 154px" id="blogTypeId" name="blogType.id" editable="false" panelHeight="auto" >
					<option value="">请选择博客类别...</option>	
				    <c:forEach var="blogType" items="${blogTypeCountList }">
				    	<option value="${blogType.id }">${blogType.typeName }</option>
				    </c:forEach>			
                </select>
   			</td>
   		</tr>
   		<tr>
   			<td valign="top">博客简介：</td>
   			<td>
   				<textarea id="summary" name="summary" style="width:850px" rows="5"></textarea>
   			</td>
   		</tr>
   		<tr>
   			<td valign="top">博客内容：</td>
   			<td>
   				<textarea id="mytmce" type="text/plain" style="width:850px;height:850px;"></textarea>
   			</td>
   		</tr>
   		<tr>
   			<td>关键字：</td>
   			<td><input type="text" id="keyWord" name="keyWord" style="width: 400px;"/>&nbsp;(多个关键字中间用空格隔开)</td>
   		</tr>
   		<tr>
   			<td></td>
   			<td>
   				<a href="javascript:submitUpdateData(${param.id})" class="easyui-linkbutton" data-options="iconCls:'icon-submit'">发布博客</a>
   			</td>
   		</tr>
   	</table>
 </div>
 
 <script type="text/javascript">
 
 $(document).ready(function(){
	 tinymce.init({
	        selector: '#mytmce',
	        upload_image_url: '${pageContext.request.contextPath}/back/blog/saveImage.do', //配置的上传图片的路由
	        height: 200,
	        menubar: false,
	        plugins: ['preview', 'link', 'codesample', 'uploadimage','fullscreen'],
	        toolbar: 'insert | undo redo  link codesample |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | uploadimage | preview | fullscreen',
	        convert_urls :false, //此设置会在保存内容图片时不改变路径  /upload/img.jpg格式，默认为true，格式为 ../upload/img.jpg
	        content_css: []
	    });
	 
	 initData();
 });

 function initData(){
	 $.post("${pageContext.request.contextPath}/back/blog/findBlogById.do",{"id":"${param.id}"},function(result){
		$("#title").val(result.title);
		$("#keyWord").val(result.keyWord);
		$("#summary").val(result.summary);
		$("#blogTypeId").combobox("setValue",result.blogType.id);
		$("#mytmce").val(result.content);
	 },"json");
 }
 
</script>
</body>
</html>

