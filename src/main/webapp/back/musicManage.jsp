<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>音乐管理页面</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/jquery-easyui-1.3.3/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/jquery-easyui-1.3.3/themes/icon.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/static/jquery-easyui-1.3.3/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/jquery-easyui-1.3.3/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/jquery-easyui-1.3.3/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/back_js/musicManage.js"></script>
<script type="text/javascript">var contextPath="${pageContext.request.contextPath}";</script>
</head>
<body style="margin: 1px">
<table id="dg" title="音乐管理" class="easyui-datagrid"
   fitColumns="true" pagination="true" rownumbers="true"
   url="${pageContext.request.contextPath}/back/music/list.do" fit="true" toolbar="#tb">
   <thead>
   	<tr>
   		<th field="cb" checkbox="true" align="center"></th>
   		<th field="id" width="20" align="center">编号</th>
   		<th field="musicName" width="200" align="center">音乐名称</th>
   		<th field="ifUse" width="200" align="center" formatter="formatIfUse">是否使用</th>
   		<th field="orderNo" width="100" align="center">排序序号</th>
   	</tr>
   </thead>
 </table>
 <div id="tb">
 	<div>
 	    <a href="javascript:openMusicAddDialog()" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加</a>
 		<a href="javascript:openMusicModifyDialog()" class="easyui-linkbutton" iconCls="icon-edit" plain="true">修改</a>
 		<a href="javascript:deleteMusic()" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a>
 	</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
 </div>
 
 <!-- 添加 -->
 <div id="dlg" class="easyui-dialog" style="width:500px;height:220px;padding: 10px 20px" closed="true" buttons="#dlg-buttons">  
   <form id="fm" method="post" enctype="multipart/form-data">
   	<table cellspacing="8px">
   		<tr>
   			<td>音乐名称：</td>
   			<td><input type="text" id="musicName" name="musicName" class="easyui-validatebox" required="true"/></td>
   		</tr>
   		<tr id="musicTemp">
   			<td>上传音乐：</td>
   			<td><input type="file" id="musicFile" name="musicFile" style="width: 200px;"/></td>
   		</tr>
   		<tr>
   			<td>是否使用：</td>
   			<td><input type="text" id="ifUse" name="ifUse" class="easyui-numberbox" required="true" style="width: 60px"/>&nbsp;(0表示"使用",1表示"不使用")</td>
   		</tr>
   		<tr>
   			<td>音乐排序：</td>
   			<td><input type="text" id="orderNo" name="orderNo" class="easyui-numberbox" required="true" style="width: 60px"/>&nbsp;(音乐根据排序序号从小到大排序)</td>
   		</tr>
   	</table>
   </form>
 </div> 
 <div id="dlg-buttons">
 	<a href="javascript:saveMusic()" class="easyui-linkbutton" iconCls="icon-ok">保存</a>
 	<a href="javascript:closeMusicDialog()" class="easyui-linkbutton" iconCls="icon-cancel">关闭</a>
 </div>
</body>
</html>