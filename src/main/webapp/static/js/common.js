var bgmObj;
$(document).ready(function () {
	bgmObj = $('#bgmAudio').get(0);
    if($.cookie("BGMPlayTime") === undefined || $.cookie("BGMPlayIndex") === undefined){
        var musicIndex = Math.floor(Math.random()*bgmList.length);
        $('#musicLabel').text(bgmList[musicIndex].name);
        console.log(bgmList[musicIndex].url)
        bgmObj.src = bgmList[musicIndex].url;
        bgmObj.currentTime = 0;
        bgmObj.load();
        bgmObj.play();

        $.cookie("BGMPlayTime",0,{path: "/"});
        $.cookie("BGMPlayIndex",musicIndex,{path: "/"});
    }else{
        $('#musicLabel').text(bgmList[parseInt($.cookie("BGMPlayIndex"))].name);
        bgmObj.src = bgmList[parseInt($.cookie("BGMPlayIndex"))].url;
        bgmObj.currentTime = parseFloat($.cookie("BGMPlayTime"));
        bgmObj.load();
        bgmObj.play();
    }


    var bgmTimer = setInterval(function () {
        console.log('音乐播放器当前状态' + bgmObj.readyState);
        if(bgmObj.readyState === 4){
            console.log('音乐播放器准备就绪，开始播放音乐');
            clearInterval(bgmTimer);
            recordBgmPlayTime();
        }
    },50);

});

function recordBgmPlayTime() {
    var recorderTimer = setInterval(function () {
        if(bgmObj.ended){
            console.log('当前音乐播放结束，开始准备下一首歌曲');
            var musicIndex = Math.floor(Math.random()*bgmList.length);
            console.log('准备播放第'+ musicIndex +'首音乐，音乐名：' + bgmList[musicIndex].name);
            $('#musicLabel').text(bgmList[musicIndex].name);
            bgmObj.src = bgmList[musicIndex].url;
            bgmObj.currentTime = 0;
            bgmObj.load();
            bgmObj.play();

            $.cookie("BGMPlayTime",0,{path: "/"});
            $.cookie("BGMPlayIndex",musicIndex,{path: "/"});
        }else{
            console.log('正在播放第'+ $.cookie("BGMPlayIndex") +'首音乐，音乐名：' + bgmList[parseInt($.cookie("BGMPlayIndex"))].name);
            $.cookie("BGMPlayTime",bgmObj.currentTime,{path: "/"});
        }
    },100);
}

function randomBGM() {
    var musicIndex = Math.floor(Math.random()*bgmList.length);

    $('#musicLabel').text(bgmList[musicIndex].name);
    bgmObj.src = bgmList[musicIndex].url;
    bgmObj.currentTime = 0;
    bgmObj.load();
    bgmObj.play();

    $.cookie("BGMPlayTime",0,{path: "/"});
    $.cookie("BGMPlayIndex",musicIndex,{path: "/"});
}