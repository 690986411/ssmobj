﻿//发布博客
function submitData(){
	var title = $("#title").val();	
	var blogTypeId = $("#blogTypeId").combobox("getValue");	
	
	//获取全部内容
	var activeEditor  = tinymce.activeEditor;	
	var content = activeEditor.getContent();
	//获取不含html标签的纯文本内容
	var contentNoTag = activeEditor.getBody().innerText;
	
	var keyWord = $("#keyWord").val();
	var summary= $("#summary").val();

	if($.trim(title)==""){alert("请输入标题!");return;}
	if($.trim(blogTypeId)==""){alert("请选择类别!");return;}
	if($.trim(content)==""){alert("请输入内容!");return;}
	if($.trim(summary)==""){alert("请输入简介!");return;}
	
	var params ={"title":title,"blogType.id":blogTypeId,"summary":summary,"content":content,"contentNoTag":contentNoTag,"keyWord":keyWord};
	
	$.post(contextPath+"/back/blog/save.do",params,function(result){
		if(result.success){
			$.messager.alert("系统提示","发布成功！");
			resetValue();//清空数据
		}else{
			$.messager.alert("系统提示","发布失败！");
		}
	},"json");	
}

//修改博客
function submitUpdateData(id){
	var title = $("#title").val();	
	var blogTypeId = $("#blogTypeId").combobox("getValue");	

	//获取全部内容
	var activeEditor  = tinymce.activeEditor;	
	var content = activeEditor.getContent();
	//获取不含html标签的纯文本内容
	var contentNoTag = activeEditor.getBody().innerText;
	
	var keyWord = $("#keyWord").val();
	var summary= $("#summary").val();
	
	if($.trim(title)==""){alert("请输入标题!");return;}
	if($.trim(blogTypeId)==""){alert("请选择类别!");return;}
	if($.trim(content)==""){alert("请输入内容!");return;}
	if($.trim(summary)==""){alert("请输入简介!");return;}
	
	var params ={"id":id,"title":title,"blogType.id":blogTypeId,"summary":summary,"content":content,"contentNoTag":contentNoTag,"keyWord":keyWord};
	$.post(contextPath+"/back/blog/save.do",params,function(result){
		if(result.success){
			$.messager.alert("系统提示","修改成功！");
			resetValue();//清空数据
		}else{
			$.messager.alert("系统提示","修改失败！");
		}
	},"json");		
}

//清空数据
function resetValue(){
	$("#title").val("");
	$("#blogTypeId").combobox("setValue","");
	tinymce.activeEditor.setContent("");
	$("#keyWord").val("");
	$("#summary").val("");
}