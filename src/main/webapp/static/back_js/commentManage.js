﻿//格式化审核状态
function formatState(val,row){
	if(val==0){
		return "待审核";
	}else if(val==1){
		return "审核通过";
	}else if(val==2){
		return "审核未通过";
	}
}
//校验标题
function formatBlogTitle(val,row){
	if(val==null){
		return "<font color='red'>该博客已被删除！</font>";
	}else{
		return "<a target='_blank' href=../front/articles/"+val.id+".html>"+val.title+"</a>";			
	}
}
//删除评论
function deleteComment(){
	var selectedRows=$("#dg").datagrid("getSelections");
	if(selectedRows.length == 0){
		$.messager.alert("系统提示","请选择要删除的数据!");
		return;
	}
	var strIds=[];
	for(var i=0;i<selectedRows.length;i++){
		strIds.push(selectedRows[i].id);
	}
	var ids = strIds.join(",");	
	$.messager.confirm("系统提示","您确定要删除这<font color=red>"+selectedRows.length+"</font>条数据吗?",function(r){
		if(r){
			$.post(contextPath+"/back/comment/delete.do",{ids:ids},function(result){
				if(result.success){
					$.messager.alert("系统提示","数据已成功删除!");
					$("#dg").datagrid("reload");
				}else{
					$.messager.alert("系统提示","数据删除失败!");
				}
			},"json");
		}
	});	
}