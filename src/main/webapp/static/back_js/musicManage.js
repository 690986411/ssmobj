﻿//打开添加音乐链接窗口
function openMusicAddDialog(){
	$("#dlg").dialog("open").dialog("setTitle","添加音乐信息");
	 $("#musicTemp").show();
	url=contextPath+"/back/music/save.do"
}
//打开修改音乐使用状态窗口
function openMusicModifyDialog(){
	var selectedRows = $("#dg").datagrid("getSelections");
	 if(selectedRows.length != 1){
		 $.messager.alert("系统提示","请选择一条要编辑的数据!");
		 return;
	 }
	 var row=selectedRows[0];
	 $("#dlg").dialog("open").dialog("setTitle","编辑音乐信息");
	 $("#musicTemp").hide();
	 $("#fm").form("load",row);
	 url=contextPath+"/back/music/save.do?id="+row.id;
}

//格式化是否使用音乐
function formatIfUse(val,row){
	if(val == 0){
		return "使用";
	}else{
		return "不使用";
	}
}

//添加友情链接
function saveMusic(){
	$("#fm").form("submit",{
		url:url,
		onSubmit:function(){
			return $(this).form("validate");
		},
		success:function(result){
			var result=eval('('+result+')');
			if(result.success){
				$.messager.alert("系统提示","保存成功！");
				resetValue();
				$("#dlg").dialog("close");
				$("#dg").datagrid("reload");
			}else{
				if(result.message){
					$.messager.alert("系统提示",result.message);
					resetValue();
					$("#dg").datagrid("reload");
				}else{
					$.messager.alert("系统提示","保存失败！");
				}			
			}
		}
	});
}
//清空数据
function resetValue(){
	 $("#musicName").val("");
	 $("#musicFile").val("");
	 $("#orderNo").val("");
}
//关闭窗口
function closeMusicDialog(){
	 $("#dlg").dialog("close");
	 resetValue();
}

//删除音乐
function deleteMusic(){
	var selectedRows=$("#dg").datagrid("getSelections");
	if(selectedRows.length == 0){
		$.messager.alert("系统提示","请选择要删除的数据!");
		return;
	}
	var strIds=[];
	for(var i=0;i<selectedRows.length;i++){
		strIds.push(selectedRows[i].id);
	}
	var ids = strIds.join(",");	
	$.messager.confirm("系统提示","您确定要删除这<font color=red>"+selectedRows.length+"</font>条数据吗?",function(r){
		if(r){
			$.post(contextPath+"/back/music/delete.do",{ids:ids},function(result){
				if(result.success){
					$.messager.alert("系统提示","数据已成功删除!");
					$("#dg").datagrid("reload");
				}else{
					$.messager.alert("系统提示","数据删除失败!");
				}
			},"json");
		}
	});	
}