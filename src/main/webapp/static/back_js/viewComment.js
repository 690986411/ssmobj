﻿$(document).ready(function(){
	hljs.initHighlightingOnLoad();
});

//校验邮箱格式
function checkEmail(){
	var emailValue = $("#email").val();
	var reemail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(!reemail.test(emailValue)){
	    alert("您输入的邮箱格式不正确,请重新输入!");
	    $("#email").val("");
	    return false;
	}
}
//发表评论
function submitData(){
	var blogId = $("#blogId").val();
	var email = $("#email").val();		
	var content = $("#content").val();
	var imageCode= $("#imageCode").val();
	
	if($.trim(email)==""){alert("请填写邮箱!");return;}
	if($.trim(content)==""){alert("请填写内容!");return;}
	if($.trim(imageCode)==""){alert("请填写验证码!");return;}
	
	var params ={"blog.id":blogId,"email":email,"content":content,"imageCode":imageCode};
	
	$.post(contextPath+"/front/saveComment.do",params,function(result){
		if(result.success){
			//window.location.reload();
			$("#email").val("");
			$("#content").val("");
			$("#imageCode").val("");
			alert("评论已提交成功，审核通过后显示！");
		}else{
			alert(result.errorMessage);
		}
	},"json");		
}
//更新验证码
function loadimage(){
	document.getElementById("randImage").src=contextPath+"/image.jsp?"+Math.random();
}
//显示更多评论
function showOtherComment(){
	$('.otherComment').show();
} 

