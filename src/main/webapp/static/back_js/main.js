﻿//刷新系统缓存
function refreshSystem(){
	$.post("${pageContext.request.contextPath}/back/system/refreshSystem.do",{},function(result){
		if(result.success){
			$.messager.alert("系统提示","已成功刷新系统缓存！");
		}else{
			$.messager.alert("系统提示","刷新系统缓存失败！");
		}
	},"json");
}