/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50612
Source Host           : localhost:3306
Source Database       : ssm_blog

Target Server Type    : MYSQL
Target Server Version : 50612
File Encoding         : 65001

Date: 2018-04-24 13:39:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_blog
-- ----------------------------
DROP TABLE IF EXISTS `t_blog`;
CREATE TABLE `t_blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `summary` varchar(400) DEFAULT NULL,
  `releaseDate` datetime DEFAULT NULL,
  `clickHit` int(11) DEFAULT NULL,
  `replyHit` int(11) DEFAULT NULL,
  `content` text,
  `typeId` int(11) DEFAULT NULL,
  `keyWord` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `typeId` (`typeId`),
  CONSTRAINT `fk_t_blog` FOREIGN KEY (`typeId`) REFERENCES `t_blogtype` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_blog
-- ----------------------------
INSERT INTO `t_blog` VALUES ('33', '我们的校园，是一个美丽的校园。', '在学校的门口，就能看到我们学校的名字——xx学校。这时，我感到无比的兴奋。因为在这里，我不仅交到了许多朋友，还让我学到了许多知识', '2018-04-23 14:20:56', '3', '0', '<p><span style=\"color: #444444; font-family: \'Microsoft YaHei\'; text-indent: 28px;\">在学校的门口，就能看到我们学校的名字&mdash;&mdash;xx学校。这时，我感到无比的兴奋。因为在这里，我不仅交到了许多</span><a style=\"color: #f70968; font-family: \'Microsoft YaHei\'; text-indent: 28px;\" href=\"http://www.zixuekaoshi.net/zuowen/pengyou/\" target=\"_blank\" rel=\"noopener\">朋友</a><span style=\"color: #444444; font-family: \'Microsoft YaHei\'; text-indent: 28px;\">，还让我学到了许多知识</span></p>\n<p><span style=\"color: #444444; font-family: \'Microsoft YaHei\'; text-indent: 28px;\"><img src=\"/upload/20180423/Desert.jpg\" width=\"540\" height=\"405\" /></span></p>\n<p style=\"margin: 0px 0px 1em; padding: 0px; text-indent: 2em; color: #444444; font-family: \'Microsoft YaHei\';\">踏入校门,映入眼帘的当然是我们学校的操场啦！操场是红绿相间的，红色的是长长的跑道，像一条腰带似的环绕着操场。绿的呢，是宽阔的篮球场。红绿互相衬托，好像一朵碧叶红瓣的花朵。</p>\n<p style=\"margin: 0px 0px 1em; padding: 0px; text-indent: 2em; color: #444444; font-family: \'Microsoft YaHei\';\">走过了操场，就是我们的旧教学楼。其中，一些是电脑室，一些是图书室，科学实验室&hellip;&hellip;旧教学楼穿的衣服是黄色的，但是经历了风风<a style=\"color: #f70968;\" href=\"http://www.zixuekaoshi.net/zuowen/yu/\" target=\"_blank\" rel=\"noopener\">雨</a><a style=\"color: #f70968;\" href=\"http://www.zixuekaoshi.net/zuowen/yu/\" target=\"_blank\" rel=\"noopener\">雨</a>，它的衣服也变得很旧了。旧教学楼的第二层就是升旗台，升旗台中间竖立着一根笔直的旗杆，五星红旗迎风飘扬，它是那样的火红。每到星期一的时候，这里便会举行升旗仪式。有四个学生高高地举起五星红旗，然后对它庄严地行礼，最后把它升上<a style=\"color: #f70968;\" href=\"http://www.zixuekaoshi.net/zuowen/tiankong/\" target=\"_blank\" rel=\"noopener\">天空</a>，仿佛像一团熊熊火焰在燃烧。新的教学楼就在旧教学楼的侧面。我们就是在那<a style=\"color: #f70968;\" href=\"http://www.zixuekaoshi.net/zuowen/xuexi/\" target=\"_blank\" rel=\"noopener\">学习</a>知识的。</p>\n<p style=\"margin: 0px 0px 1em; padding: 0px; text-indent: 2em; color: #444444; font-family: \'Microsoft YaHei\';\">我们的校园就像一个大家园，多么美丽，多么可爱，我们在这里茁壮<a style=\"color: #f70968;\" href=\"http://www.zixuekaoshi.net/zuowen/chengzhang/\" target=\"_blank\" rel=\"noopener\">成长</a>。</p>', '12', '校园 成长');
INSERT INTO `t_blog` VALUES ('34', 'java代码', '测试java代码', '2018-04-23 14:22:28', '2', '0', '<p>测试java代码</p>\n<pre class=\"language-java\"><code>	\n	/**\n	 * 删除对应博客索引\n	 * @param string\n	 */\n	public void deleteIndex(String blogId)throws Exception {\n		IndexWriter writer=getWriter();\n		writer.deleteDocuments(new Term(\"id\",blogId));\n		writer.forceMergeDeletes(); // 强制删除\n		writer.commit();\n		writer.close();\n		\n	}\n</code></pre>', '11', '代码  java');
INSERT INTO `t_blog` VALUES ('35', '我们的校园就像个美丽的大公园。', '进入校门，就看见两张豪华的大沙发，沙发对面还有一排排的书架，书架上有各式各样的书。这里就是等候区。', '2018-04-23 14:26:11', '11', '4', '<p><span style=\"color: #444444; font-family: \'Microsoft YaHei\'; text-indent: 28px;\">进入校门，就看见两张豪华的大沙发，沙发对面还有一排排的书架，书架上有各式各样的书。这里就是等候区。</span></p>\n<p><span style=\"color: #444444; font-family: \'Microsoft YaHei\'; text-indent: 28px;\"><img src=\"/upload/20180423/Hydrangeas.jpg\" width=\"385\" height=\"289\" /></span></p>\n<p style=\"margin: 0px 0px 1em; padding: 0px; text-indent: 2em; color: #444444; font-family: \'Microsoft YaHei\';\">等候区前面有一条小路，路中央有一个金鱼池。金鱼池里有很多各式各样、颜色徇丽多彩的金鱼，有大有小。金鱼池的两旁还有不同的花草树木，连起来真是一个美丽的风景！</p>\n<p style=\"margin: 0px 0px 1em; padding: 0px; text-indent: 2em; color: #444444; font-family: \'Microsoft YaHei\';\">沿着小路一直向前走，就有一排排的石柱，每根石柱前都有一个转角大沙发。大沙发前就是楼梯了！楼梯的空位都被设为图书角了，书架上有学校购买的新书也有<a style=\"color: #f70968;\" href=\"http://www.zixuekaoshi.net/zuowen/tongxue/\" target=\"_blank\" rel=\"noopener\">同学</a>们捐的书，各式各样！</p>\n<p style=\"margin: 0px 0px 1em; padding: 0px; text-indent: 2em; color: #444444; font-family: \'Microsoft YaHei\';\">沿着楼梯走，来到二楼，就是我们的<a style=\"color: #f70968;\" href=\"http://www.zixuekaoshi.net/zuowen/jiaoshis/\" target=\"_blank\" rel=\"noopener\">教室</a>了。一共有十三个<a style=\"color: #f70968;\" href=\"http://www.zixuekaoshi.net/zuowen/jiaoshis/\" target=\"_blank\" rel=\"noopener\">教室</a>，每个教室里都有黑板、触屏电脑、大<a style=\"color: #f70968;\" href=\"http://www.zixuekaoshi.net/zuowen/dianshi/\" target=\"_blank\" rel=\"noopener\">电视</a>机、图书角、墙壁和几十张桌子和椅子。</p>\n<p style=\"margin: 0px 0px 1em; padding: 0px; text-indent: 2em; color: #444444; font-family: \'Microsoft YaHei\';\"><img src=\"/upload/20180423/Tulips.jpg\" width=\"474\" height=\"355\" /></p>\n<p style=\"margin: 0px 0px 1em; padding: 0px; text-indent: 2em; color: #444444; font-family: \'Microsoft YaHei\';\">教室的左边的正下面就是一个羽毛球场。有两个羽毛球网。大家打羽毛球就在这里。教室的右边正下面是一个美丽的大花园。有两张乒乓球台，和几张石凳，石凳两旁还有几棵高大的树。</p>\n<p style=\"margin: 0px 0px 1em; padding: 0px; text-indent: 2em; color: #444444; font-family: \'Microsoft YaHei\';\">花园的前面就是操场了！操场里有个大足球场，也有一个跑道。操场前面有一个<a style=\"color: #f70968;\" href=\"http://www.zixuekaoshi.net/zuowen/wutai/\" target=\"_blank\" rel=\"noopener\">舞台</a>，<a style=\"color: #f70968;\" href=\"http://www.zixuekaoshi.net/zuowen/laoshi/\" target=\"_blank\" rel=\"noopener\">老师</a>们演讲就在<a style=\"color: #f70968;\" href=\"http://www.zixuekaoshi.net/zuowen/wutai/\" target=\"_blank\" rel=\"noopener\">舞台</a>上。</p>\n<p style=\"margin: 0px 0px 1em; padding: 0px; text-indent: 2em; color: #444444; font-family: \'Microsoft YaHei\';\">我们的校园到处有美丽的景色，说也说不尽，<a style=\"color: #f70968;\" href=\"http://www.zixuekaoshi.net/zuowen/xiwang/\" target=\"_blank\" rel=\"noopener\">希望</a>你能来细细游赏！</p>', '12', '校园  ');

-- ----------------------------
-- Table structure for t_blogtype
-- ----------------------------
DROP TABLE IF EXISTS `t_blogtype`;
CREATE TABLE `t_blogtype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderNo` int(11) DEFAULT NULL,
  `typeName` varchar(30) DEFAULT NULL,
  `blogCount` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_blogtype
-- ----------------------------
INSERT INTO `t_blogtype` VALUES ('11', '1', 'java', null);
INSERT INTO `t_blogtype` VALUES ('12', '2', '杂谈', null);
INSERT INTO `t_blogtype` VALUES ('13', '3', '人生', null);

-- ----------------------------
-- Table structure for t_comment
-- ----------------------------
DROP TABLE IF EXISTS `t_comment`;
CREATE TABLE `t_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userIp` varchar(50) DEFAULT NULL,
  `content` varchar(1000) DEFAULT NULL,
  `blogId` int(11) DEFAULT NULL,
  `commentDate` datetime DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_comment
-- ----------------------------
INSERT INTO `t_comment` VALUES ('7', '0:0:0:0:0:0:0:1', '＜script＞alert(“333“);＜/script＞', '35', '2018-04-24 09:05:46', '1', 'dd@qq.com');
INSERT INTO `t_comment` VALUES ('8', '192.168.1.3', '＜script＞alert(“11“)＜/script＞', '35', '2018-04-24 13:14:03', '1', '101@qq.com');

-- ----------------------------
-- Table structure for t_link
-- ----------------------------
DROP TABLE IF EXISTS `t_link`;
CREATE TABLE `t_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `linkName` varchar(255) DEFAULT NULL,
  `linkUrl` varchar(255) DEFAULT NULL,
  `orderNo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_link
-- ----------------------------
INSERT INTO `t_link` VALUES ('3', 'kingkone博客', 'https://kinglone.com', '1');
INSERT INTO `t_link` VALUES ('4', '博客', 'https://kinglone.com', '2');

-- ----------------------------
-- Table structure for t_music
-- ----------------------------
DROP TABLE IF EXISTS `t_music`;
CREATE TABLE `t_music` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `musicName` varchar(60) DEFAULT NULL,
  `musicUrl` varchar(255) DEFAULT NULL,
  `ifUse` int(1) DEFAULT NULL,
  `orderNo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_music
-- ----------------------------
INSERT INTO `t_music` VALUES ('7', '实施', '/upload/music/20180424/Kalimba.mp3', '0', '1');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `sysid` int(11) NOT NULL AUTO_INCREMENT,
  `imageurl` varchar(255) DEFAULT NULL,
  `nickname` varchar(30) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `profile` varchar(255) DEFAULT NULL,
  `sign` varchar(255) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`sysid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', null, null, 'dd881704ef6a92165e5e643dc62901a9', null, '青春无非是一场梦,得拼得闯得奋斗!', 'kinglone');
